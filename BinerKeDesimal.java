import java.util.Scanner;

public class BinerKeDesimal {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("--------------------------");
        String biner;

        do {
            System.out.print("Masukkan bilangan biner : ");
            biner = sc.nextLine();
            if (!cekDigitBiner(biner)) {
                System.out.println("Bilangan biner tidak valid. Silahkan coba lagi.");
            }
        } while (!cekDigitBiner(biner));

        int desimal = 0;
        for (int i = biner.length() - 1; i >= 0; i--) {
            int digit = biner.charAt(i) - '0';
            desimal += digit * Math.pow(2, biner.length() - 1 - i);
        }
        System.out.println("--------------------------");

        System.out.println("Hasil konversi : " + desimal);
        sc.close();
    }

    public static boolean cekDigitBiner(String input) {
        return input.matches("[01]+");
    }
}
