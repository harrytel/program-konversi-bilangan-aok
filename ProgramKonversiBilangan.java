import java.util.Scanner;

public class ProgramKonversiBilangan {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int pilihan;
        do {
            tampilkanMenu();
            try {
                pilihan = sc.nextInt();
                sc.nextLine(); // Membuang karakter newline dari buffer

                switch (pilihan) {
                    case 1:
                        konversiBinerKeDesimal(sc);
                        break;
                    case 2:
                        konversiDesimalKeBiner(sc);
                        break;
                    case 3:
                        konversiBinerKeHexadesimal(sc);
                        break;
                    case 4:
                        konversiHexadesimalKeBiner(sc);
                        break;
                    case 5:
                        konversiDesimalKeHexadesimal(sc);
                        break;
                    case 6:
                        konversiHexadesimalKeDesimal(sc);
                        break;
                    case 0:
                        System.out.println("-------------------------------------------------");
                        System.out.println("Terima kasih! Program selesai.");
                        System.out.println("-------------------------------------------------");
                        break;
                    default:
                        System.out.println("-------------------------------------------------");
                        System.out.println("Pilihan tidak valid. Silakan pilih kembali.");
                        break;
                }
            } catch (java.util.InputMismatchException e) {
                System.out.println("-------------------------------------------------");
                System.out.println("Pilihan tidak valid. Silakan pilih nomor dari menu.");
                sc.nextLine(); // Membersihkan buffer input.
                pilihan = -1; // Memberi nilai default untuk melanjutkan loop
            }
        } while (pilihan != 0);

        sc.close();
    }

    public static void tampilkanMenu() {
        System.out.println("=================================================");
        System.out.println("            PROGRAM KONVERSI BILANGAN            ");
        System.out.println("=================================================");
        System.out.println("MENU : ");
        System.out.println("0.  KELUAR");
        System.out.println("1.  BINER KE DESIMAL");
        System.out.println("2.  DESIMAL KE BINER");
        System.out.println("3.  BINER KE HEXADESIMAL");
        System.out.println("4.  HEXADESIMAL KE BINER");
        System.out.println("5.  DESIMAL KE HEXADESIMAL");
        System.out.println("6.  HEXADESIMAL KE DESIMAL");
        System.out.println("--------------------------");
        System.out.print("MASUKKAN PILIHAN ANDA  : ");
    }

    public static void konversiBinerKeDesimal(Scanner sc) {
        System.out.println("--------------------------");
        String biner;

        do {
            System.out.print("Masukkan bilangan biner : ");
            biner = sc.nextLine();
            if (!cekDigitBiner(biner)) {
                System.out.println("Bilangan biner tidak valid. Silahkan coba lagi.");
            }
        } while (!cekDigitBiner(biner));

        int desimal = 0;
        for (int i = biner.length() - 1; i >= 0; i--) {
            int digit = biner.charAt(i) - '0';
            desimal += digit * Math.pow(2, biner.length() - 1 - i);
        }
        System.out.println("--------------------------");

        System.out.println("Hasil konversi : " + desimal);

    }

    public static boolean cekDigitBiner(String input) {
        return input.matches("[01]+");
    }

    public static void konversiDesimalKeBiner(Scanner sc) {
        int desimal;
        do {
            System.out.println("--------------------------");
            System.out.print("Masukkan bilangan desimal : ");
            try {
                desimal = sc.nextInt();
                System.out.println("--------------------------");
                if (desimal < 0) {
                    System.out.println("Bilangan desimal tidak boleh negatif.");
                }
            } catch (java.util.InputMismatchException e) {
                System.out.println("Input yang Anda masukkan bukan bilangan desimal.");
                sc.nextLine(); // Membersihkan buffer input
                desimal = -1; // Memberi nilai default untuk melanjutkan loop
            }
        } while (desimal < 0);

        int sisa;
        String sisaA;
        String biner = "";
        do {
            sisa = desimal % 2;
            desimal /= 2;
            sisaA = Integer.toString(sisa);
            biner = sisaA + biner;
        } while (desimal != 0);
        System.out.println("Hasil konversi : " + biner);
    }

    public static void konversiBinerKeHexadesimal(Scanner sc) {
        System.out.println("--------------------------");
        System.out.print("Masukkan bilangan biner : ");
        String biner = sc.next();
        System.out.println("--------------------------");

        while (!cekDigitBiner(biner)) {
            System.out.println("Bilangan biner tidak valid. Silahkan coba lagi.");
            System.out.print("Masukkan bilangan biner : ");
            biner = sc.next();
        }

        int desimal = BinerKeDesimal(biner);
        String hexadesimal = DesimalKeHexa(desimal);
        System.out.println("Hasil konversi: " + hexadesimal);
    }

    public static void konversiHexadesimalKeBiner(Scanner sc) {
        String hexadesimal;
        do {
            System.out.println("--------------------------");
            System.out.print("Masukkan bilangan hexadesimal : ");
            hexadesimal = sc.nextLine();
            System.out.println("--------------------------");
            hexadesimal = hexadesimal.toUpperCase();
            if (!hexadesimal.matches("^[0-9A-F]+$")) {
                System.out.println("Bilangan hexadesimal tidak valid.");
            }
        } while (!hexadesimal.matches("^[0-9A-F]+$"));

        int desimal = HexaKeDesimal(hexadesimal);
        String biner = DesimalKeBiner(desimal);

        System.out.println("Hasil konversi: " + biner);
        System.out.println("--------------------------");
    }

    public static void konversiDesimalKeHexadesimal(Scanner sc) {
        int desimal;
        System.out.println("--------------------------");

        while (true) {
            try {
                System.out.print("Masukkan bilangan desimal: ");
                desimal = sc.nextInt();
                if (desimal < 0) {
                    System.out.println("Bilangan desimal tidak boleh negatif.");
                } else {
                    break; // Jika input benar, keluar dari loop.
                }
            } catch (java.util.InputMismatchException e) {
                System.out.println("Input tidak valid. Masukkan bilangan desimal yang valid.");
                sc.next(); // Membersihkan buffer input.
            }
        }

        int sisa;
        String sisaA;
        String hexa = "";
        System.out.print("Hasil konversi : ");

        do {
            sisa = desimal % 16;
            desimal /= 16;
            sisaA = Integer.toString(sisa);
            if (sisa == 10) {
                hexa = "A" + hexa;
            } else if (sisa == 11) {
                hexa = "B" + hexa;
            } else if (sisa == 12) {
                hexa = "C" + hexa;
            } else if (sisa == 13) {
                hexa = "D" + hexa;
            } else if (sisa == 14) {
                hexa = "E" + hexa;
            } else if (sisa == 15) {
                hexa = "F" + hexa;
            } else {
                hexa = sisaA + hexa;
            }
        } while (desimal != 0);
        System.out.println(hexa);
    }

    public static void konversiHexadesimalKeDesimal(Scanner sc) {
        System.out.println("--------------------------");
        String hexadesimal, hexa;
        do {
            System.out.print("Masukkan bilangan hexadesimal : ");
            hexadesimal = sc.next();
            System.out.println("--------------------------");
            hexa = hexadesimal.toUpperCase();
            if (!hexa.matches("^[0-9A-F]+$")) {
                System.out.println("Bilangan hexadesimal tidak valid.");
            }
        } while (!hexa.matches("^[0-9A-F]+$"));
        int nilai;
        int desimal = 0;
        System.out.print("Hasil konversi : ");

        for (int i = hexa.length(); i > 0; i--) {
            int digit = hexa.charAt(i - 1);
            if (digit == '0') {
                nilai = 0;
            } else if (digit == '1') {
                nilai = 1;
            } else if (digit == '2') {
                nilai = 2;
            } else if (digit == '3') {
                nilai = 3;
            } else if (digit == '4') {
                nilai = 4;
            } else if (digit == '5') {
                nilai = 5;
            } else if (digit == '6') {
                nilai = 6;
            } else if (digit == '7') {
                nilai = 7;
            } else if (digit == '8') {
                nilai = 8;
            } else if (digit == '9') {
                nilai = 9;
            } else if (digit == 'A') {
                nilai = 10;
            } else if (digit == 'B') {
                nilai = 11;
            } else if (digit == 'C') {
                nilai = 12;
            } else if (digit == 'D') {
                nilai = 13;
            } else if (digit == 'E') {
                nilai = 14;
            } else {
                nilai = 15;
            }
            desimal += (nilai * (Math.pow(16, hexa.length() - i)));
        }
        System.out.println(desimal);
    }

    public static int BinerKeDesimal(String biner) {
        int desimal = 0;
        for (int i = biner.length() - 1; i >= 0; i--) {
            int digit = biner.charAt(i) - '0';
            desimal += digit * Math.pow(2, biner.length() - 1 - i);
        }
        return desimal;
    }

    public static String DesimalKeHexa(int desimal) {
        int sisa;
        String sisaA;
        String hexa = "";
        do {
            sisa = desimal % 16;
            desimal /= 16;
            sisaA = Integer.toString(sisa);
            if (sisa == 10) {
                hexa = "A" + hexa;
            } else if (sisa == 11) {
                hexa = "B" + hexa;
            } else if (sisa == 12) {
                hexa = "C" + hexa;
            } else if (sisa == 13) {
                hexa = "D" + hexa;
            } else if (sisa == 14) {
                hexa = "E" + hexa;
            } else if (sisa == 15) {
                hexa = "F" + hexa;
            } else {
                hexa = sisaA + hexa;
            }
        } while (desimal != 0);
        return hexa;
    }

    public static int HexaKeDesimal(String hexa) {
        int nilai;
        int desimal = 0;
        for (int i = hexa.length(); i > 0; i--) {
            int digit = hexa.charAt(i - 1);
            if (digit == '0') {
                nilai = 0;
            } else if (digit == '1') {
                nilai = 1;
            } else if (digit == '2') {
                nilai = 2;
            } else if (digit == '3') {
                nilai = 3;
            } else if (digit == '4') {
                nilai = 4;
            } else if (digit == '5') {
                nilai = 5;
            } else if (digit == '6') {
                nilai = 6;
            } else if (digit == '7') {
                nilai = 7;
            } else if (digit == '8') {
                nilai = 8;
            } else if (digit == '9') {
                nilai = 9;
            } else if (digit == 'A') {
                nilai = 10;
            } else if (digit == 'B') {
                nilai = 11;
            } else if (digit == 'C') {
                nilai = 12;
            } else if (digit == 'D') {
                nilai = 13;
            } else if (digit == 'E') {
                nilai = 14;
            } else {
                nilai = 15;
            }
            desimal += (nilai * (Math.pow(16, hexa.length() - i)));
        }
        return desimal;
    }

    public static String DesimalKeBiner(int desimal) {
        int sisa;
        String sisaA;
        String biner = "";
        do {
            sisa = desimal % 2;
            desimal /= 2;
            sisaA = Integer.toString(sisa);
            biner = sisaA + biner;
        } while (desimal != 0);
        return biner;
    }
}