import java.util.Scanner;

public class HexaKeDesimal {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("--------------------------");
        String hexadesimal, hexa;
        do {
            System.out.print("Masukkan bilangan hexadesimal : ");
            hexadesimal = sc.next();
            System.out.println("--------------------------");
            hexa = hexadesimal.toUpperCase();
            if (!hexa.matches("^[0-9A-F]+$")) {
                System.out.println("Bilangan hexadesimal tidak valid.");
            }
        } while (!hexa.matches("^[0-9A-F]+$"));
        int nilai;
        int desimal = 0;
        System.out.print("Hasil konversi : ");

        for (int i = hexa.length(); i > 0; i--) {
            int digit = hexa.charAt(i - 1);
            if (digit == '0') {
                nilai = 0;
            } else if (digit == '1') {
                nilai = 1;
            } else if (digit == '2') {
                nilai = 2;
            } else if (digit == '3') {
                nilai = 3;
            } else if (digit == '4') {
                nilai = 4;
            } else if (digit == '5') {
                nilai = 5;
            } else if (digit == '6') {
                nilai = 6;
            } else if (digit == '7') {
                nilai = 7;
            } else if (digit == '8') {
                nilai = 8;
            } else if (digit == '9') {
                nilai = 9;
            } else if (digit == 'A') {
                nilai = 10;
            } else if (digit == 'B') {
                nilai = 11;
            } else if (digit == 'C') {
                nilai = 12;
            } else if (digit == 'D') {
                nilai = 13;
            } else if (digit == 'E') {
                nilai = 14;
            } else {
                nilai = 15;
            }
            desimal += (nilai * (Math.pow(16, hexa.length() - i)));
            // 2B
            /*
             * B -> 11 ===> 0 + 11* 16^0 = 11*1 = 11
             * 2 -> 2 ===> 11 + 2* 16^1 = 2* 16 = 32
             * total 11 + 32 =43
             */
        }
        System.out.println(desimal);
        sc.close();
    }

}
