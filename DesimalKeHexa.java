import java.util.Scanner;

public class DesimalKeHexa {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int desimal;
        System.out.println("--------------------------");

        while (true) {
            try {
                System.out.print("Masukkan bilangan desimal: ");
                desimal = sc.nextInt();
                if (desimal < 0) {
                    System.out.println("Bilangan desimal tidak boleh negatif.");
                } else {
                    break; // Jika input benar, keluar dari loop.
                }
            } catch (java.util.InputMismatchException e) {
                System.out.println("Input tidak valid. Masukkan bilangan desimal yang valid.");
                sc.next(); // Membersihkan buffer input.
            }
        }

        int sisa;
        String sisaA;
        String hexa = "";
        System.out.print("Hasil konversi : ");

        do {
            sisa = desimal % 16;
            desimal /= 16;
            sisaA = Integer.toString(sisa);
            if (sisa == 10) {
                hexa = "A" + hexa;
            } else if (sisa == 11) {
                hexa = "B" + hexa;
            } else if (sisa == 12) {
                hexa = "C" + hexa;
            } else if (sisa == 13) {
                hexa = "D" + hexa;
            } else if (sisa == 14) {
                hexa = "E" + hexa;
            } else if (sisa == 15) {
                hexa = "F" + hexa;
            } else {
                hexa = sisaA + hexa;
            }
        } while (desimal != 0);
        /*
         * 43
         * hexa : "2B"
         * 43 : 16 = 2 sisa 11 -> B
         * 2 : 16 = 0 sisa 2 -> 2
         * Hexa : 2B
         * 2 11
         */
        System.out.println(hexa);

        sc.close();
    }
}
