import java.util.Scanner;

public class HexaKeBiner {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String hexadesimal;
        do {
            System.out.println("--------------------------");
            System.out.print("Masukkan bilangan hexadesimal : ");
            hexadesimal = sc.nextLine();
            System.out.println("--------------------------");
            hexadesimal = hexadesimal.toUpperCase();
            if (!hexadesimal.matches("^[0-9A-F]+$")) {
                System.out.println("Bilangan hexadesimal tidak valid.");
            }
        } while (!hexadesimal.matches("^[0-9A-F]+$"));

        int desimal = HexaKeDesimal(hexadesimal);
        String biner = DesimalKeBiner(desimal);

        System.out.println("Hasil konversi: " + biner);
        System.out.println("--------------------------");
        sc.close();
    }

    public static int HexaKeDesimal(String hexa) {
        // hexa = 2B
        int nilai;
        int desimal = 0;
        for (int i = hexa.length(); i > 0; i--) {
            int digit = hexa.charAt(i - 1);
            if (digit == '0') {
                nilai = 0;
            } else if (digit == '1') {
                nilai = 1;
            } else if (digit == '2') {
                nilai = 2;
            } else if (digit == '3') {
                nilai = 3;
            } else if (digit == '4') {
                nilai = 4;
            } else if (digit == '5') {
                nilai = 5;
            } else if (digit == '6') {
                nilai = 6;
            } else if (digit == '7') {
                nilai = 7;
            } else if (digit == '8') {
                nilai = 8;
            } else if (digit == '9') {
                nilai = 9;
            } else if (digit == 'A') {
                nilai = 10;
            } else if (digit == 'B') {
                nilai = 11;
            } else if (digit == 'C') {
                nilai = 12;
            } else if (digit == 'D') {
                nilai = 13;
            } else if (digit == 'E') {
                nilai = 14;
            } else {
                nilai = 15;
            }
            desimal += (nilai * (Math.pow(16, hexa.length() - i)));
        }
        /*
         * 2B
         * B = 11 -> desimal = 11* 16^0 = 11
         * 2 -> desimal = 2 * 16^1 = 32
         * TOtal = 43(desimal)
         */
        return desimal;
    }

    public static String DesimalKeBiner(int desimal) {
        int sisa;
        String sisaA;
        String biner = "";
        do {
            sisa = desimal % 2;
            desimal /= 2;
            sisaA = Integer.toString(sisa);
            biner = sisaA + biner;
        } while (desimal != 0);
        /*
         * 11
         * 43 : 2 = 21 sisa 1
         * 21 : 2 = 10 sisa 1
         * 10 : 2 = 5 sisa 0
         * 5 : 2 = 2 sisa 1
         * 2 : 2 = 1 sisa 0
         * 1 : 2 = 0 sisa 1
         * biner : 101011
         */
        return biner;
    }
}
