import java.util.Scanner;

public class BinerKeHexa {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("--------------------------");
        System.out.print("Masukkan bilangan biner : ");
        String biner = sc.next(); // 101011
        System.out.println("--------------------------");

        while (!cekDigitBiner(biner)) {
            System.out.println("Bilangan biner tidak valid. Silahkan coba lagi.");
            System.out.print("Masukkan bilangan biner : ");
            biner = sc.next();
        }

        int desimal = BinerKeDesimal(biner);
        String hexadesimal = DesimalKeHexa(desimal);
        System.out.println("Hasil konversi: " + hexadesimal);
        sc.close();
    }

    public static boolean cekDigitBiner(String input) {
        return input.matches("[01]+");
    }

    public static int BinerKeDesimal(String biner) {
        int desimal = 0;
        for (int i = biner.length() - 1; i >= 0; i--) {
            int digit = biner.charAt(i) - '0';
            desimal += digit * Math.pow(2, biner.length() - 1 - i);
        }
        /*
         * 101011 :
         * 1 -> 1.1 = 1
         * 1 -> 1.2 = 2
         * 0 -> 0.4 = 0
         * 1 -> 1.8 = 8
         * 0 -> 0.16 = 0
         * 1 -> 1.32 = 32
         * Total = 43(desimal)
         */

        return desimal;
    }

    public static String DesimalKeHexa(int desimal) {
        int sisa;
        String sisaA;
        String hexa = "";
        /*
         * 43 : 16 = 2 sisa 11 -> B
         * 2 : 16 = 0 sisa 2 -> 2
         * Hexa : 2B
         * 2 11
         */
        do {
            sisa = desimal % 16;
            desimal /= 16;
            sisaA = Integer.toString(sisa);
            if (sisa == 10) {
                hexa = "A" + hexa;
            } else if (sisa == 11) {
                hexa = "B" + hexa;
            } else if (sisa == 12) {
                hexa = "C" + hexa;
            } else if (sisa == 13) {
                hexa = "D" + hexa;
            } else if (sisa == 14) {
                hexa = "E" + hexa;
            } else if (sisa == 15) {
                hexa = "F" + hexa;
            } else {
                hexa = sisaA + hexa;
            }
        } while (desimal != 0);
        return hexa;
    }
}
