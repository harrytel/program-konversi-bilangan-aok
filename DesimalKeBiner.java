import java.util.Scanner;

public class DesimalKeBiner {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int desimal;
        do {
            System.out.println("--------------------------");
            System.out.print("Masukkan bilangan desimal : ");
            try {
                desimal = sc.nextInt();
                System.out.println("--------------------------");
                if (desimal < 0) {
                    System.out.println("Bilangan desimal tidak boleh negatif.");
                }
            } catch (java.util.InputMismatchException e) {
                System.out.println("Input yang Anda masukkan bukan bilangan desimal.");
                sc.nextLine(); // Membersihkan buffer input
                desimal = -1; // Memberi nilai default untuk melanjutkan loop
            }
        } while (desimal < 0);

        int sisa;
        String sisaA;
        String biner = "";
        do {
            sisa = desimal % 2;
            desimal /= 2;
            sisaA = Integer.toString(sisa);
            biner = sisaA + biner;
        } while (desimal != 0);
        System.out.println("Hasil konversi : " + biner);
        sc.close();
    }
}
